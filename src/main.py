import logging
logging.basicConfig(level=logging.DEBUG,
                            format="%(name)s %(asctime)s : %(filename)s[LINE:%(lineno)d] : %(levelname)s : %(message)s",
                            datefmt='%m-%d %H:%M:%S',
                            handlers=[logging.FileHandler("./log.log",mode="w"),logging.StreamHandler()])

logging.getLogger("PIL.PngImagePlugin").setLevel(logging.CRITICAL)
logging.getLogger("matplotlib.pyplot").setLevel(logging.CRITICAL)
logging.getLogger("matplotlib.font_manager").setLevel(logging.CRITICAL)

import random
import multiprocessing
import numpy as np
import matplotlib.pyplot as plt
from typing import Type

from PN.proportional_navigation import PN, PNOptions
from PN.models import HeadingVelocity


def simulation(pursuer: Type[HeadingVelocity], target: Type[HeadingVelocity], options: Type[PNOptions] = PNOptions(True, True, True), N: int = 3, dt: int = 1, min_r:int=1 ):
    terminate = False
    t = 0

    rets = []

    log = {'pursuer':{'x':[],'y':[]},'target':{'x':[],'y':[]}}
    while not terminate:
        ret = PN(pursuer,target,N=N,options=options).calculate()

        J = ret['J']
        R = ret['R']
        Vc = ret['Vc']

        t = t+dt
        ret['t'] = t
        if int(R) < min_r: #or t > 400:
            terminate = True

        psipd = np.rad2deg(J/Vc)
        ret['j/Vc'] = J/(Vc*ret["W"])
        # logging.info("psi:%s, psipd:%s", pursuer.psi, psipd)
        old_psi = pursuer.psi
        pursuer.psi = pursuer.psi + psipd*dt
        # logging.info("old psi:%s, new psi:%s, psipd:%s, dt:%s", old_psi, pursuer.psi, psipd, dt)

        pursuer.x += pursuer.xd*dt
        pursuer.y += pursuer.yd*dt
        target.x += target.xd*dt
        target.y += target.yd*dt

        log['pursuer']['x'].append(pursuer.x)
        log['pursuer']['y'].append(pursuer.y)
        log['target']['x'].append(target.x)
        log['target']['y'].append(target.y)
        logging.info(ret)

        plt.plot(log['pursuer']['y'], log['pursuer']['x'],"ro")
        plt.plot(log['target']['y'], log['target']['x'], 'bo')
        rets.append(ret)
        plt.pause(0.0001)
    logging.info(rets)
    plt.show()


def gen_aircraft(count:int):

    allow_psi = [0,45,90,180,270]

    for _ in range(0,count):
        speed = random.randint(2,5)
        p_x = random.uniform(-50,50)
        p_y = random.uniform(-50,50)

        t_x = random.uniform(-50,50)
        t_y = random.uniform(-50,50)

        p_psi = random.choice(allow_psi)
        t_psi = random.choice(allow_psi)

        pursuer = HeadingVelocity(p_psi, p_x, p_y,speed)
        target = HeadingVelocity(t_psi, t_x, t_y, speed-1)

    return (pursuer, target)



if __name__ == "__main__":
    # pursuer = HeadingVelocity(0,100,200,8)
    # target = HeadingVelocity(270,-100,500,5)
    # pursuer = HeadingVelocity(90,0,0,3)
    # target = HeadingVelocity(270,100,50,2)

    options = PNOptions(True,True,True,True,True)
    dt = 1
    N = 3

    # simulation(pursuer, target, options,N)
    # gen_simulation(4)
    pursuer, target = gen_aircraft(1)
    logging.info("pursuer:%s, target:%s",pursuer, target)
    simulation(pursuer, target, options, N, dt, 3)


